﻿using Discord;
using Discord.Commands;
using Discord.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
namespace DiscordConsole
{
    class Program
    {
        static DiscordClient _client = new DiscordClient(); //new DiscordClientConfig() { LogLevel = LogMessageSeverity.Debug }
        static void Main(string[] args)
        {
            Console.Title = "SeanBot - a Discord Bot"; //bool parrot = false;
            var client = _client; 
            DateTime startTime = new DateTime();
            //Log some information
            client.LogMessage += (s, e) => Console.WriteLine($"[{e.Severity}] [{e.Source}]: {e.Message}");

            //Setting up command service
            CommandService cS = new CommandService(new CommandServiceConfig()
            { CommandChar = null, HelpMode = HelpMode.Disable });
            client.AddService(cS);
            
            //Help Command
            cS.CreateCommand("~help")
                .Alias(new string[] { "~h", "@SeanBot give me commands", "@SeanBot help me", "@SeanBot give me help!", "@SeanBot help plz", "~commands", "@SeanBot commands", "@SeanBot h" })
                .Parameter("none", ParameterType.Unparsed)
                .Description("Help command?")
                .Do(async e =>
                {
                    string helpstr = string.Empty; 
                    foreach (var com in client.Commands().AllCommands)
                    {
                        if (!com.IsHidden)
                        {
                            helpstr += $"{Format.Bold("Command:")}  {Format.Code(com.Text)}. Aliases: ";
                            helpstr += $"{Format.Code(string.Join(", ", com.Aliases))}";
                            helpstr += $"\n{Format.Bold("Description")}: {com.Description}\n\n";
                        }
                    }
                    while (helpstr.Length > 2000)
                    {
                        await client.SendPrivateMessage(e.User, helpstr.Substring(0, 2000));
                        helpstr = helpstr.Substring(2000);
                    }
                    await client.SendPrivateMessage(e.User, helpstr);
                    await client.SendMessage(e.Channel, $"{Mention.User(e.User)}! sent you a private message!");
                });

            //Command Service is creating a command, the parameter is test. It all makes sense now.
            cS.CreateCommand("~grab")
                .Description("Test function, just for Sean.")
                .Parameter("user", ParameterType.Unparsed)
                .Alias("~contact")
                .Hide()
                .Do(async e =>
                {
                    if (e.User.HasRole(client.GetRole(97675818183958528)) || e.Server.Owner == e.User || e.User.Id == 118203879551664137) //That ID right there is server-only.
                    {
                        var _users = client.FindUsers(e.Server, e.Args[0]).ToArray();
                        foreach (User u in _users)
                            await client.SendMessage(e.Channel, $"{Mention.User(u)}, you're stupid! -{Format.Italics(e.User.Name)}");
                    }
                    else
                        await client.SendMessage(e.Channel, "Hey! You can't do that! :anger:");
                });

            cS.CreateCommand("~setrole")
                .Alias("@SeanBot setrole")
                .Parameter("user role", ParameterType.Multiple)
                .Description("Admin command. Sets role of a user.")
                .Do(async e =>
                {
                    if (e.User.HasRole(client.GetRole(97675818183958528)) || e.User == e.Server.Owner)
                    {
                        var _u = client.FindUsers(e.Server, e.Args[0]).ToArray();
                        var _r = client.FindRoles(e.Server, e.Args[1]).ToArray();
                        foreach (User u in _u)
                        {
                            foreach (Role r in _r)
                            {
                                if (!u.HasRole(r))
                                {
                                    IEnumerable<Role> newRole = u.Roles;
                                    await client.EditUser(u, false, false, newRole.Union(_r));
                                    //Verify we changed the roles of the user
                                    string roles = string.Join(", ", u.Roles.Where(x => !x.IsEveryone));
                                    string info = $"Name: {u.Name}\nStatus: {u.Status}\nRoles: {roles}\nID: {u.Id}\nJoin date: {u.JoinedAt}\nLast online: {u.LastOnlineAt}\n";
                                    await client.SendMessage(e.Channel, Format.Code(info));
                                }
                            }
                        }
                    }
                    else
                        await client.SendMessage(e.Channel, "Hey! You can't do that! :anger:");
                });
            cS.CreateCommand("~mute")
                .Alias(new string[] { "@Seanbot mute", "@SeanBot m" })
                .Description("Mutes channels")
                .Parameter("voice channel", ParameterType.Unparsed)
                .Hide()
                .Do(async e =>
                {
                    if (e.User.HasRole(client.GetRole(97675818183958528)) || e.User == e.Server.Owner)
                    {
                        var _c = client.FindChannels(e.Server, e.Args[0], ChannelType.Voice, false);
                        foreach (Channel c in _c)
                        {
                            var _u = c.Members; IEnumerable<Role> r;
                            foreach (User u in _u)
                            {
                                r = u.Roles;
                                await client.EditUser(u, true, false, r);
                            }
                                await client.SendMessage(e.Channel, "Muted! :trumpet:");
                        }
                        //await client.SendMessage(e.Channel, "Working on it~");
                    }
                });
            cS.CreateCommand("~unmute")
                .Alias(new string[] { "@SeanBot unmute", "@SeanBot unm" })
                .Description("Unmute channels")
                .Parameter("voice channel", ParameterType.Unparsed)
                .Hide()
                .Do(async e =>
                {
                    var _c = client.FindChannels(e.Server, e.Args[0], ChannelType.Voice, false);
                    foreach (Channel c in _c)
                    {
                        var _u = c.Members; IEnumerable<Role> r;
                        foreach (User u in _u)
                        {
                            r = u.Roles;
                            await client.EditUser(u, false, false, r);
                        }
                        await client.SendMessage(e.Channel, "Unmuted! :trumpet:");
                    }
                });

            cS.CreateCommand("~invite")
                .Alias("@SeanBot invite")
                .Parameter("inviteorxkcdcode", ParameterType.Required)
                .Description("Send an server invite code to SeanBot")
                .Hide()
                .Do(async e =>
                {
                    await client.AcceptInvite(client.GetInvite(e.Args[0]).Result);
                    await client.SendMessage(e.Channel, $"Invite to {e.Args[0]} has been accepted. :D");
                });

            cS.CreateCommand("~say")
                .Alias(new string[] { "@SeanBot say", "@Seanbot s", "@SeanBot echo", "@SeanBot e", "~echo" })
                .Description("Echoes desired text.")
                .Parameter("say", ParameterType.Unparsed)
                .Do(async e =>
                {
                    await client.SendMessage(e.Channel, e.Args[0]);
                });

            cS.CreateCommand("~poi")
                .Alias(new string[] { "@SeanBot poi~", "@SeanBot poi" })
                .Description("poi~")
                .Parameter("none", ParameterType.Multiple)
                .Hide()
                .Do(async e =>
                    { await client.SendTTSMessage(e.Channel, "poipoipoipoipoipoipoipoipoipoi~"); });

            cS.CreateCommand("~roll")
                .Alias(new string[] { "@Seanbot roll", "@SeanBot r" })
                .Description("Dice roller. Doesn't support 1d20+x yet!")
                .Parameter("#d#", ParameterType.Multiple)
                .Do(async e =>
                {
                    //taking from ~roll 1d20
                    //Rolling once, from values between 1 to 20, including 1 and 20
                    try
                    {
                        Random rnd = new Random(); Regex reg = new Regex(@"^([0-9])d([0-9])\+([0-9])");
                        string fmt = Format.Normal(e.Message.Text);
                        if (fmt == "@SeanBot roll")
                            fmt = "@SeanBot roll 1d20";
                        if (fmt == "@SeanBot r")
                            fmt = "@SeanBot r 1d20";
                        if (fmt == "~roll")
                            fmt = "~roll 1d20";
                        Match m = reg.Match(fmt);
                        switch (m.Success)
                        {
                            case true:
                                int Min = 1; int Max = int.Parse(m.Groups[2].Value) + 1;
                                int[] test = Enumerable
                                    .Repeat(0, int.Parse(m.Groups[1].Value))
                                    .Select(i => rnd.Next(Min, Max))
                                    .ToArray();
                                await client.SendMessage(e.Channel, $"{ e.User } has rolled\n{Format.Code(string.Join(", ", test))}");
                                break;

                            case false:
                                if (e.Message.Text.Contains("a blunt"))
                                    await client.SendMessage(e.Channel, "Proceeding to roll blunt.. :evergreen_tree: :smoking: :evergreen_tree: :smoking:");
                                break;
                        }
                    }
                    catch (FormatException ex)
                    {
                        await client.SendMessage(e.Channel, $"`Error: {ex.Message}`\nWhy would you even try that?! :anger:");
                    }
                });

            cS.CreateCommand("~rekt")
                .Alias("rekt")
                .Description("Shows you how REKT you are. ")
                .Parameter("rekt", ParameterType.Optional)
                .Do(async e =>
                {
                    string msg = "  ☐  Not REKT\n☑ REK\n☑ Really Rekt\n☑ REKTangle\n☑ SHREKT\n☑ REKT-it Ralph\n☑ Total REKTall\n☑ The Lord of the REKT\n☑ The Usual SusREKTs\n☑ North by NorthREKT\n☑ REKT to the Future\n☑ Once Upon a Time in the REKT\n☑ Full mast erektion\n☑ Rektum\n☑ Resurrekt\n☑ CorRekt\n☑ Indirekt\n☑ Tyrannosaurus Rekt";
                    await client.SendMessage(e.Channel, msg);
                });

            cS.CreateCommand("~bestgirl")
                .Alias("@SeanBot show me best girl")
                .Description("showing love to best girl")
                .Parameter("bestgirl", ParameterType.Optional)
                .Hide()
                .Do(async e =>
                {
                    //My internet is too slow for this. x.x
                    await client.SendMessage(e.Channel, "I'll show you who's the best girl!");
                    await client.SendMessage(e.Channel, "http://i.imgur.com/Yj7ZzXn.png");
                });

            cS.CreateCommand("~tts")
                .Alias(new string[] { "@SeanBot tts" })
                .Parameter("tts", ParameterType.Multiple)
                .Description("Sends TTS message.")
                .Hide()
                .Do(async e =>
                {
                    await client.SendTTSMessage(e.Channel, string.Concat(e.Args));
                });

            cS.CreateGroup("", stats =>
            {
                stats.CreateCommand("~admins")
                    .Alias(new string[] { "@SeanBot give me admins", "@SeanBot admins" })
                    .Description("Grabs list of known admins")
                    .Parameter("admins", ParameterType.Optional)
                    .Do(async e =>
                    {
                        string admins = string.Empty;
                        if (e.Server.Id == 97674113845301248)
                            admins = $"{e.Server.Owner.Name}";
                        foreach (User u in e.Server.Members)
                            foreach (Role r in u.Roles)
                                if (r.Name.Contains("Adminshit"))
                                    admins += $", {u.Name}";
                        await client.SendMessage(e.Channel, $"{Format.Code(admins)}");
                    });

                stats.CreateCommand("~mstats")
                    .Alias(new string[] { "~memberstats", "@SeanBot mstats", "@SeanBot give me member" })
                    .Description("Member Stats")
                    .Parameter("memberstats", ParameterType.Optional)
                    .Do(async e =>
                    {
                        int int_online; int int_idle; int int_offline; int _total; string str_online; string str_idle; string str_offline;
                        int_online = e.Server.Members.Where(x => x.Status == UserStatus.Online).Count();
                        int_idle = e.Server.Members.Where(x => x.Status == UserStatus.Idle).Count();
                        int_offline = e.Server.Members.Where(x => x.Status == UserStatus.Offline).Count();
                        _total = int_online + int_idle + int_offline;

                        str_online = string.Join(", ", e.Server.Members.Where(x => x.Status == UserStatus.Online));
                        str_idle = string.Join(", ", e.Server.Members.Where(x => x.Status == UserStatus.Idle));
                        str_offline = string.Join(", ", e.Server.Members.Where(x => x.Status == UserStatus.Offline.Value));

                        string info = $"Server: {e.Server.Name}\n";
                        info += $"| Online: { int_online } | Idle: { int_idle } | Offline: { int_offline } | Total: { _total } |\n";
                        info += $"Members Online: {str_online}\n";
                        info += $"Members Idle: {str_idle}\n";
                        info += $"Members Offline: {str_offline}";
                        await client.SendMessage(e.Channel, Format.Code(info));
                    });

                stats.CreateCommand("~uptime")
                    .Alias(new string[] { "@SeanBot uptime", "@SeanBot how long you been running" })
                    .Description("How long has Seanbot been running")
                    .Parameter("uptime", ParameterType.Optional)
                    .Do(async e =>
                    {
                        var later = DateTime.Now - startTime;
                        string msg = Format.Bold($"{Math.Floor(later.TotalHours).ToString("n0")}h {later.Minutes.ToString("n0")}m {later.Seconds.ToString("n0") }s");
                        await client.SendMessage(e.Channel, $"SeanBot has been up for {msg}.");
                    });
                stats.CreateCommand("~version")
                    .Alias(new string[] { "@SeanBot v", "@SeanBot version" })
                    .Description("Displays the current version SeanBot is on. :poop:")
                    .Parameter("version", ParameterType.Optional)
                    .Do(async e =>
                    {
                        await client.SendMessage(e.Channel, $"SeanBot is at version 1.0.2.8! :poop:");
                    });
                stats.CreateCommand("~serverstats")
                    .Alias(new string[] { "~sstats", "@SeanBot server stats" })
                    .Description("Displays server stats.")
                    .Parameter(string.Empty, ParameterType.Multiple)
                    .Do(async e =>
                    {
                        string msg = $"Server: {e.Server.Name}\nOwner: {e.Server.Owner.Name}\nID: {e.Server.Id}\nRegion: {e.Server.Region}\nMembers: {e.Server.Members.Count()}\n";
                        msg += $"Channels: {e.Server.TextChannels.Count()} Text/ {e.Server.VoiceChannels.Count()} Voice\nRoles: {string.Join(", ", e.Server.Roles.Where(x => !x.IsEveryone))}";
                        await client.SendMessage(e.Channel, Format.Code(msg));
                    });
                stats.CreateCommand("~whois")
                    .Alias(new string[] { "@SeanBot whois", "@SeanBot dox", "~dox" })
                    .Description("Displays requested user information.")
                    .Parameter("user", ParameterType.Unparsed)
                    .Do(async e =>
                    {
                        string msg = Format.Normal(e.Args[0]); //Testing without substring
                        var _user = client.FindUsers(e.Server, msg);
                        foreach (User u in _user)
                        {
                            string roles = string.Join(", ", u.Roles.Where(x => !x.IsEveryone));
                            string info = $"Name: {u.Name}\nStatus: {u.Status}\nRoles: {roles}\nID: {u.Id}\nJoin date: {u.JoinedAt}\nLast online: {u.LastOnlineAt}\n";
                            await client.SendMessage(e.Channel, Format.Code(info));
                        }
                    });
            });

            cS.CreateCommand("~resean")
                .Description("Restarts SeanBot~")
                .Alias(new string[] { "~restart", "@SeanBot restart", "@SeanBot resean" })
                .Parameter("resean", ParameterType.Optional)
                .Parameter("restart", ParameterType.Optional)
                .Do(async e =>
                {
                    await client.SendMessage(e.Channel, "ReSeaning..");
                    var fileName = Assembly.GetExecutingAssembly().Location;
                    System.Diagnostics.Process.Start(fileName);
                    Environment.Exit(0);
                });

            cS.CreateCommand("femalesean")
                .Alias("fsean")
                .Description("The words of the female Sean.")
                .Parameter("femalesean", ParameterType.Multiple)
                .Hide()
                .Do(async e =>
                {
                    await client.SendMessage(e.Channel, "Dick or gtfo.");
                });

            cS.CreateCommand("parrot")
                .Alias("caw")
                .Description("caw")
                .Parameter("parrot", ParameterType.Unparsed)
                .Hide()
                .Do(async e =>
                {
                    await client.SendMessage(e.Channel, "http://static2.fjcdn.com/comments/I+definitely+did+not+expect+bird+ahegao+in+this+anime+_2be4b6cfb4c0df15e8032bfd7f6ee844.png");
                });

            client.MessageReceived += async (s, e) =>
            {
                string msg = e.Message.RawText;
                if (!e.Message.IsAuthor)
                {
                    switch (msg.ToLower())
                    {
                        case "kek":
                            await client.SendMessage(e.Channel, Format.Italics("kek"));
                            break;
                        case "meme":
                            await client.SendMessage(e.Channel, Format.Bold("SEAN IS THE ULTIMATE MEMELORD. :trumpet: :trumpet: :trumpet: :trumpet: :trumpet: :trumpet: :trumpet:"));
                            break;
                        case "spooky":
                            await client.SendMessage(e.Channel, ":skull: :trumpet: :skull: :trumpet: :skull: :trumpet: :skull: :trumpet: :skull: :trumpet: ");
                            break;
                        case "ayy":
                            await client.SendMessage(e.Channel, "░░░░█▒▒▄▀▀▀▀▀▄▄▒▒▒▒▒▒▒▒▒▄▄▀▀▀▀▀▀▄\n░░▄▀▒▒▒▄█████▄▒█▒▒▒▒▒▒▒█▒▄█████▄▒█\n░█▒▒▒▒▐██▄████▌▒█▒▒▒▒▒█▒▐██▄████▌▒█\n▀▒▒▒▒▒▒▀█████▀▒▒█▒░▄▒▄█▒▒▀█████▀▒▒▒█\n▒▒▐▒▒▒░░░░▒▒▒▒▒█▒░▒▒▀▒▒█▒▒▒▒▒▒▒▒▒▒▒▒█\n▒▌▒▒▒░░░▒▒▒▒▒▄▀▒░▒▄█▄█▄▒▀▄▒▒▒▒▒▒▒▒▒▒▒▌\n▒▌▒▒▒▒░▒▒▒▒▒▒▀▄▒▒█▌▌▌▌▌█▄▀▒▒▒▒▒▒▒▒▒▒▒▐\n▒▐▒▒▒▒▒▒▒▒▒▒▒▒▒▌▒▒▀███▀▒▌▒▒▒▒▒▒▒▒▒▒▒▒▌\n▀▀▄▒▒▒▒▒▒▒▒▒▒▒▌▒▒▒▒▒▒▒▒▒▐▒▒▒▒▒▒▒▒▒▒▒█\n▀▄▒▀▄▒▒▒▒▒▒▒▒▐▒▒▒▒▒▒▒▒▒▄▄▄▄▒▒▒▒▒▒▄▄▀\n▒▒▀▄▒▀▄▀▀▀▄▀▀▀▀▄▄▄▄▄▄▄▀░░░░▀▀▀▀▀▀\n▒▒▒▒▀▄▐▒▒▒▒▒▒▒▒▒▒▒▒▒▐\n░▄▄▄░░▄░░▄░▄░░▄░░▄░░░░▄▄░▄▄░░░▄▄▄░░░▄▄▄\n█▄▄▄█░█▄▄█░█▄▄█░░█░░░█░░█░░█░█▄▄▄█░█░░░█\n█░░░█░░█░░░░█░░░░█░░░█░░█░░█░█░░░█░█░░░█\n▀░░░▀░░▀░░░░▀░░░░▀▀▀░░░░░░░░░▀░░░▀░▀▄▄▄▀");
                            break;
                        case "rip seanbot!":
                            await client.SendMessage(e.Channel, "Well... I gotta go now. Bye~");
                            Environment.Exit(0);
                            break;
                        case "(╯°□°）╯︵ ┻━┻":
                            await client.SendMessage(e.Channel, "QUIT FLIPPING MY TABLE!!!\n┬─┬﻿ ノ( ゜-゜ノ)");
                            break;
                        case @"im sorry": //Work on this later.
                            await client.SendMessage(e.Channel, $"{Mention.User(e.User)} You better be.");
                            break;
                        case "ping":
                            await client.SendMessage(e.Channel, $"{Mention.User(e.User)} pong!");
                            break;
                        case "wew":
                            await client.SendMessage(e.Channel, "wew lad");
                            break;
                    }
                }
            };

            //Convert our sync method to an async one and blocks this function until the client disconnects
            client.Run(async () =>
            {
                //Connect to the Discord server using our email and password
                await client.Connect("seanisabot@gmail.com", "SeanBot1567");
                startTime = DateTime.Now;
                //If we are not a member of any server
                if (!client.AllServers.Any())
                    await client.AcceptInvite(client.GetInvite("0XpEeY3oeB0E0fRs").Result);
            });
        }

    }
}
